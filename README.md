This is a spawn plugin developed by IntelInside for demos.

It contains some nice features, such as:
- Configurable Messages (Found in config, supports color codes)
- /setspawn
- Configurable Spawn Point (Found in config)

Command Reference:
- /spawn; Requires "spawn.use"
- /setspawn; Requires "spawn.set"
