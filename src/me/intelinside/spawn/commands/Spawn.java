package me.intelinside.spawn.commands;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import me.intelinside.spawn.Main;

public class Spawn implements CommandExecutor {
	public Main plugin;
	
	public Spawn(Main main) {
		this.plugin = main;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(cmd.getName().equalsIgnoreCase("spawn")) {
			if(sender.hasPermission("spawn.use")) {
				Location loc = new Location(Bukkit.getWorld(plugin.config.getString("world")),
						plugin.config.getDouble("x"), 
						plugin.config.getDouble("y"), 
						plugin.config.getDouble("z"), 
						(float) plugin.config.getDouble("yaw"), 
						(float) plugin.config.getDouble("pitch"));
				Bukkit.getPlayer(sender.getName()).teleport(loc);
				sender.sendMessage(plugin.sentToSpawnMsg);
			}
			else {
				sender.sendMessage(plugin.noPermMsg);
			}
		}
		return false;
	}

}
