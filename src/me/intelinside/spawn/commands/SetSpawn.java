package me.intelinside.spawn.commands;

import java.io.IOException;

import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.intelinside.spawn.Main;

public class SetSpawn implements CommandExecutor {
	public Main plugin;
	
	public SetSpawn(Main main) {
		this.plugin = main;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(cmd.getName().equalsIgnoreCase("setspawn")) {
			if(sender instanceof Player) {
				if(sender.hasPermission("spawn.set")) {
					Location loc = ((Player) sender).getLocation();
					plugin.config.set("world", ((Player) sender).getWorld().getName());
					plugin.config.set("x", loc.getX());
					plugin.config.set("y", loc.getY());
					plugin.config.set("z", loc.getZ());
					plugin.config.set("yaw", (double) loc.getYaw());
					plugin.config.set("pitch", (double) loc.getPitch());
					try {
						plugin.config.save(plugin.file);
					} catch (IOException e) {
						e.printStackTrace();
					}
					sender.sendMessage(plugin.setSpawnMsg);
				}
				else {
					sender.sendMessage(plugin.noPermMsg);
				}
			}
			else {
				sender.sendMessage("You must be a player to use this command.");
			}
		}
		return false;
	}

}
