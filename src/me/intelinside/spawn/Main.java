package me.intelinside.spawn;

import java.io.File;

import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import me.intelinside.spawn.commands.SetSpawn;
import me.intelinside.spawn.commands.Spawn;

public class Main extends JavaPlugin {
	public World world;
	public FileConfiguration config;
	public File file;
	public String setSpawnMsg;
	public String sentToSpawnMsg;
	public String noPermMsg;
	
	@Override
	public void onEnable() {
		config = getConfig();
		config.options().copyDefaults(true);
		saveConfig();
		file = new File(getDataFolder(), "config.yml");
		setSpawnMsg = ChatColor.translateAlternateColorCodes("&".charAt(0), this.config.getString("setSpawnMsg"));
		sentToSpawnMsg = ChatColor.translateAlternateColorCodes("&".charAt(0), this.config.getString("sentToSpawnMsg"));
		noPermMsg = ChatColor.translateAlternateColorCodes("&".charAt(0), this.config.getString("noPermMsg"));
		this.getCommand("setspawn").setExecutor(new SetSpawn(this));
		this.getCommand("spawn").setExecutor(new Spawn(this));
	}
}
